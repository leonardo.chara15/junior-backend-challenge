import {Module} from '@nestjs/common'
import {AppController} from './app.controller'
import {AppService} from './app.service'
import {DatabaseModule} from './database/database.module'
import {setEnvironment} from './environments'
import {ConfigModule} from '@nestjs/config'
import {ScheduleModule} from '@nestjs/schedule'
import {HitsModule} from './features/hits/hits.module'
import {HttpModule} from '@nestjs/axios'

@Module({
	imports: [
		ConfigModule.forRoot({
			envFilePath: setEnvironment(),
			isGlobal: true,
		}),
		DatabaseModule,
		ScheduleModule.forRoot(),
		HttpModule.register({}),
		HitsModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {
}
