import {Module} from '@nestjs/common'
import {HitsController} from './controllers/hits.controller'
import {HitsService} from './services/hits.service'
import {TypeOrmModule} from '@nestjs/typeorm'
import {Hit} from './entities/hit.entity'
import {HttpModule} from '@nestjs/axios'

@Module({
	imports: [
		TypeOrmModule.forFeature([Hit]),
		HttpModule,
	],
	providers: [HitsService],
	controllers: [HitsController]
})
export class HitsModule {
}
