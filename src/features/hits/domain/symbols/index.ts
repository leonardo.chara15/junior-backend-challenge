export interface HitsResponse {
	hits: HitI[]
	nbHits: number
	page: number
	nbPages: number
	hitsPerPage: number
	exhaustiveNbHits: boolean
	exhaustiveTypo: boolean
	exhaustive: Exhaustive
	query: string
	params: string
	processingTimeMS: number
	processingTimingsMS: ProcessingTimingsMs
	serverTimeMS: number
}

export interface HitI {
	created_at: string
	title?: string
	url?: string
	author: string
	points?: number
	story_text?: string
	comment_text?: string
	num_comments?: number
	story_id?: number
	story_title?: string
	story_url?: string
	parent_id?: number
	created_at_i: number
	_tags: string[]
	objectID: string
	_highlightResult: HighlightResult
}

export interface HighlightResult {
	author: Author
	comment_text?: CommentText
	story_title?: StoryTitle
	story_url?: StoryUrl
	title?: Title
	story_text?: StoryText
	url?: Url
}

export interface Author {
	value: string
	matchLevel: string
	matchedWords: any[]
}

export interface CommentText {
	value: string
	matchLevel: string
	fullyHighlighted: boolean
	matchedWords: string[]
}

export interface StoryTitle {
	value: string
	matchLevel: string
	matchedWords: any[]
}

export interface StoryUrl {
	value: string
	matchLevel: string
	matchedWords: string[]
	fullyHighlighted?: boolean
}

export interface Title {
	value: string
	matchLevel: string
	matchedWords: any[]
}

export interface StoryText {
	value: string
	matchLevel: string
	fullyHighlighted: boolean
	matchedWords: string[]
}

export interface Url {
	value: string
	matchLevel: string
	fullyHighlighted: boolean
	matchedWords: string[]
}

export interface Exhaustive {
	nbHits: boolean
	typo: boolean
}

export interface ProcessingTimingsMs {
	afterFetch: AfterFetch
	fetch: Fetch
	request: Request
	total: number
}

export interface AfterFetch {
	format: Format
	total: number
}

export interface Format {
	highlighting: number
	total: number
}

export interface Fetch {
	scanning: number
	total: number
}

export interface Request {
	queue: number
	roundTrip: number
}

export type HitsFilter = {
	author: string | null
	tags: string[] | null
	title: string | null
}
