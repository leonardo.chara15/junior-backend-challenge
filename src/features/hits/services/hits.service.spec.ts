import {HitsService} from './hits.service'
import {Test, TestingModule} from '@nestjs/testing'
import {Hit} from '../entities/hit.entity'
import {getRepositoryToken} from '@nestjs/typeorm'
import {HttpService} from '@nestjs/axios'

const hitsRepoMock = {
	findOneBy: jest.fn(),
	softDelete: jest.fn(),
}

describe('Test hits service', function () {

	let service: HitsService

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				HitsService,
				{
					provide: getRepositoryToken(Hit),
					useValue: hitsRepoMock
				},
				{
					provide: HttpService,
					useValue: {}
				}
			]
		}).compile()

		service = module.get(HitsService)
	})

	it('should delete a hit and return it', async () => {
		const hit = new Hit()
		hit.objectID = 'asdas123123asd'

		jest.spyOn(hitsRepoMock, 'findOneBy').mockResolvedValue(hit)
		jest.spyOn(hitsRepoMock, 'softDelete')

		const deletedHit = await service.deleteHitByObjectIdOrFail('asdas123123asd')
		expect((deletedHit as Hit).objectID).toBe('asdas123123asd')

	})

	it('should try to delete and return an exception if not found', async () => {

		jest.spyOn(hitsRepoMock, 'findOneBy').mockResolvedValue(null)
		jest.spyOn(hitsRepoMock, 'softDelete')

		await expect(service.deleteHitByObjectIdOrFail('asdas123123asd')).rejects.toThrow()
	})
})
