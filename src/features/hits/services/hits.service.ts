import {Injectable, Logger, NotFoundException} from '@nestjs/common'
import {HttpService} from '@nestjs/axios'
import {HitI, HitsFilter, HitsResponse} from '../domain/symbols'
import {Result} from 'typescript-result'
import {Hit} from '../entities/hit.entity'
import {ILike, Repository} from 'typeorm'
import {Cron, CronExpression} from '@nestjs/schedule'
import {InjectRepository} from '@nestjs/typeorm'
import {Page, PageMeta, PageOptions} from '../../../shared/page-options'

@Injectable()
export class HitsService {

	private logger = new Logger('HitsService')

	constructor(
		private readonly httpService: HttpService,
		@InjectRepository(Hit)
		private readonly repo: Repository<Hit>,
	) {
	}

	@Cron(CronExpression.EVERY_HOUR)
	private async saveHitsInDB() {
		this.logger.log('--------- starting sync --------')
		const result = await this.fetchHits()
		if (result.isFailure()) {
			this.logger.error('Cannot sync hits', result.error)
			return
		}

		const hits = this.createHitEntities(result.value)
		await this.storeInChunk(hits)
		this.logger.log('Successfully hits sync')
	}

	private async fetchHits(): Promise<Result<Error, HitI[]>> {
		try {
			const {data} = await this.httpService.axiosRef
			.get<HitsResponse>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
			return Result.ok(data.hits)
		} catch (e) {
			this.logger.error('An error ocurred!', e)
			return Result.error(e)
		}
	}

	private createHitEntities(hits: HitI[]): Hit[] {
		return hits.map((hitI) => {
			const hit = new Hit()
			hit.storyId = hitI.story_id
			hit.storyTitle = hitI.story_title
			hit.storyUrl = hitI.story_url
			hit.parentId = hitI.parent_id
			hit.createdAtI = hitI.created_at_i
			hit.tags = hitI._tags
			hit.objectID = hitI.objectID
			hit.highlightResult = hitI._highlightResult
			hit.num_comments = hitI.num_comments
			hit.comment_text = hitI.comment_text
			hit.story_text = hitI.story_text
			hit.points = hitI.points
			hit.author = hitI.author
			hit.url = hitI.url
			hit.title = hitI.title
			hit.created_at = hitI.created_at
			return hit
		})
	}

	private storeInChunk(hits: Hit[]): Promise<Hit[]> {
		return this.repo.save(hits, {chunk: 10})
	}

	public async getHits(pagOpts: PageOptions, filter: HitsFilter) {
		const qb = this.repo.createQueryBuilder('hit')

		if (filter.author !== null) {
			qb.where('LOWER(hit.author) LIKE LOWER(:author)', {author: `%${filter.author}%`})
		}

		if (filter.title !== null) {
			qb.andWhere('LOWER(hit.title) LIKE LOWER(:title)', {title: `%${filter.title}%`})
		}

		if (filter.tags !== null) {
			qb.andWhere('hit.tags @> :tags', {tags: JSON.stringify(filter.tags)})
		}

		qb.skip(pagOpts.skip)
		  .take(pagOpts.take)

		const [hits, count] = await qb.getManyAndCount()
		const pageMeta = new PageMeta(pagOpts, count)
		return new Page(hits, pageMeta)
	}

	public async deleteHitByObjectIdOrFail(id: string) {
		const hit = await this.repo.findOneBy({objectID: id})
		if (!hit) throw new NotFoundException('')

		await this.repo.softDelete({objectID: id})
		return hit
	}

	public async deleteAll() {
		await this.repo.delete({objectID: ILike('%%')})
	}
}
