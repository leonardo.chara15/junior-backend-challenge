import {Body, Controller, Delete, HttpCode, Param, Post, Query} from '@nestjs/common'
import {HitsService} from '../services/hits.service'
import {GetHitsPageDto} from '../dtos'
import {QueryPageDto} from '../../../shared/query-page.dto'
import {PageOptions} from '../../../shared/page-options'
import {ApiBody, ApiOperation, ApiTags} from '@nestjs/swagger'

@ApiTags('Hits')
@Controller('hits')
export class HitsController {

	constructor(private readonly histService: HitsService) {
	}

	@ApiOperation({summary: 'get a page of hits'})
	@ApiBody({type: GetHitsPageDto})
	@HttpCode(200)
	@Post()
	async getHits(
		@Query() query: QueryPageDto,
		@Body() dto: GetHitsPageDto,
	) {
		return this.histService.getHits(
			new PageOptions(query.page, query.take), {
				author: dto.author,
				tags: dto.tags,
				title: dto.title
			}
		)
	}

	@ApiOperation({summary: 'delete a hit by ObjectId'})
	@Delete(':hitObjectId')
	async deleteHitByObjectId(
		@Param('hitObjectId') hitObjectId: string
	) {
		return this.histService.deleteHitByObjectIdOrFail(hitObjectId)
	}

	@ApiOperation({summary: 'delete all hits from the database'})
	@Delete()
	async deleteAllHits() {
		return this.histService.deleteAll()
	}
}
