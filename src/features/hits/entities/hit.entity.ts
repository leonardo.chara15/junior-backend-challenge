import {Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryColumn, UpdateDateColumn} from 'typeorm'
import {HighlightResult} from '../domain/symbols'

@Entity('Hits')
export class Hit {

	@PrimaryColumn()
	objectID: string

	@Column({type: 'integer', nullable: true})
	storyId: number | null

	@Column({type: 'text', nullable: true})
	storyTitle: string | null

	@Column({type: 'text', nullable: true})
	storyUrl: string | null

	@Column({type: 'integer', nullable: true})
	parentId: number | null

	@Column({type: 'integer'})
	createdAtI: number

	@Column({type: 'jsonb'})
	tags: string[]

	@Column({type: 'jsonb'})
	highlightResult: HighlightResult

	@Column({type: 'integer', nullable: true})
	num_comments: number | null

	@Column({type: 'text', nullable: true})
	comment_text: string | null

	@Column({type: 'text', nullable: true})
	story_text: string | null

	@Column({type: 'integer', nullable: true})
	points: number | null

	@Column({type: 'text'})
	author: string

	@Column({type: 'text', nullable: true})
	url: string | null

	@Column({type: 'text', nullable: true})
	title: string | null

	@Column({type: 'text'})
	created_at: string

	@CreateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
	createdAt: Date

	@UpdateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
	editedAt: Date

	@DeleteDateColumn({type: 'timestamp', nullable: true, default: null})
	deletedAt: Date | null
}
