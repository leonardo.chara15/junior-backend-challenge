import {IsString, ValidateIf} from 'class-validator'
import {ApiProperty} from '@nestjs/swagger'

export class GetHitsPageDto {
	@ApiProperty({nullable: true})
	@IsString()
	@ValidateIf((object, value) => value !== null)
	readonly author: string | null

	@ApiProperty({nullable: true, isArray: true, type: 'string'})
	@IsString({each: true})
	@ValidateIf((object, value) => value !== null)
	readonly tags: string[] | null

	@ApiProperty({nullable: true})
	@IsString()
	@ValidateIf((object, value) => value !== null)
	readonly title: string | null
}
