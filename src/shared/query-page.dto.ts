import {IsNumber, Max, Min} from 'class-validator'
import {Transform} from 'class-transformer'
import {ApiProperty} from '@nestjs/swagger'

export class QueryPageDto {

	@ApiProperty({minimum: 1})
	@Transform(({ value }) => +value)
	@Min(1)
	@IsNumber()
	page: number

	@ApiProperty({maximum: 5})
	@Transform(({ value }) => +value)
	@Max(5)
	@IsNumber()
	take: number
}
