import {PageMeta, PageOptions} from './page-options'

describe('Test page options', () => {

	it('should skip be 0', function () {
		const pageOpts = new PageOptions(1, 5)
		expect(pageOpts.skip).toBe(0)

	})

	it('should skip be 5',  () => {
		const pageOpts = new PageOptions(2, 5)
		expect(pageOpts.skip).toBe(5)
	})

	it('should skip be 25', () => {
		const pageOpts = new PageOptions(5, 5)
		expect(pageOpts.skip).toBe(20)
	})
})

describe('Test page', () => {

	it('should pageCount = 2, hasPreviousPage = false, hasNextPage = true', () => {
		const pageOpts = new PageOptions(1, 5)

		const pageMeta = new PageMeta(pageOpts, 10)

		expect(pageMeta.pageCount).toBe(2)
		expect(pageMeta.hasPreviousPage).toBe(false)
		expect(pageMeta.hasNextPage).toBe(true)
	})

	it('should pageCount = 2, hasPreviousPage = true, hasNextPage = false', function () {
		const pageOpts = new PageOptions(2, 5)

		const pageMeta = new PageMeta(pageOpts, 10)

		expect(pageMeta.pageCount).toBe(2)
		expect(pageMeta.hasPreviousPage).toBe(true)
		expect(pageMeta.hasNextPage).toBe(false)
	})
})
