import * as process from 'process'

// this creates a merge between files for dotenv
export function setEnvironment() {
	switch (process.env.NODE_ENV) {
		case 'testing':
			return ['.env.testing', '.env']
		case 'development':
			return ['.env.development']
		case 'production':
			return '.env'
		default:
			return ['.env.development', '.env.local']
	}
}

export function getEnv(): string {
	return process.env.NODE_ENV ? process.env.NODE_ENV : 'development'
}
