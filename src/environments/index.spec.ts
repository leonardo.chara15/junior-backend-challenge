import {getEnv, setEnvironment} from './index'
import * as process from 'process'


describe('Env variables tests', () => {

	let originalEnv = undefined

	beforeAll(() => {
		originalEnv = process.env.NODE_ENV
	})

	it('if NODE_ENV not defined return marge of development and local', () => {
		const env = setEnvironment()
		expect(env).toEqual(['.env.development', '.env.local'])
	})

	it('should be .env.development ', function () {
		process.env.NODE_ENV = 'development'
		const env = setEnvironment()
		expect(env).toEqual(['.env.development'])
	})

	it('should be .env.testing .env', function () {
		process.env.NODE_ENV = 'testing'
		const env = setEnvironment()
		expect(env).toEqual(['.env.testing', '.env'])
	})

	it('should be .env (production)', function () {
		process.env.NODE_ENV = 'production'
		const env = setEnvironment()
		expect(env).toEqual('.env')
	})

	afterAll(() => {
		process.env.NODE_ENV = originalEnv
	})
})


describe('Get current NODE_ENV', () => {

	const originalEnv = process.env.NODE_ENV

	beforeAll(() => {
		delete process.env.NODE_ENV
	})

	it('should return development if not ENV defined', () => {
		const env = getEnv()
		expect(env).toEqual('development')
	})

	afterAll(() => {
		process.env.NODE_ENV = originalEnv
	})
})
