
## Description

Hello, I am going to tell you how to run this app

1. First you need to clone the repo:


```bash
$ git clone https://gitlab.com/leonardo.chara15/junior-backend-challenge.git
```

2. second create a database with the name backend-junior. If you want to rename it you can change the environment variables of the .env files listed below (in this case .env.development and .env.local)

```
PORT=3000
TZ=UTC

DB_HOST=localhost
DB_NAME=backend-junior
DB_PORT=5432
DB_USERNAME=root
DB_PASSWORD=rivendel3
DB_SYNCHRONIZE_ENTITIES=false
```
3. install dependencies and run the app

```bash
$ npm install
$ npm start:dev
```

4. run tests ```run npm test:cov```

## Routes
There are 3 routes:
- Get page (gets the page by applying filters, the filters use the AND logic, I attach an example in the postman request): http://localhost:3000/hits?page=1&take=5

- Delete by ObjectId (delete a hit by ObjectId): http://localhost:3000/hits/:hitObjectId

- Delete All (deletes all records saved in the Hits table to start again):
  http://localhost:3000/hits

## Clear records to start over
invoke the DELETE endpoint http://localhost:3000/hits


